package com.demoyork.mvcdemo.models;

public class mdl_Product {
    // Declare fields class
    private int code;
    private String name;
    private double price;
    private int categ;

    // Default constructor method
    public mdl_Product() {
    }
    
    // Constructor method with arguments
    public mdl_Product(int code, String name, double price, int categ) {
        this.code = code;
        this.name = name;
        this.price = price;
        this.categ = categ;
    }
    
    // Accessor methods (getters/setters)
    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public int getCateg() {
        return categ;
    }

    public void setCateg(int categ) {
        this.categ = categ;
    }
    
    
}
