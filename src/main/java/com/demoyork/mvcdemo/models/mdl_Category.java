package com.demoyork.mvcdemo.models;

public class mdl_Category {
    // Declare fields class
    private int code;
    private String name;
    private String description;

    // Default constructor method
    public mdl_Category() { }

    // Constructor method with arguments
    public mdl_Category(int cod, String nam, String des) {
        this.code = cod;
        this.name = nam;
        this.description = des;
    }   

    // Accessor methods (getters/setters)
    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }    
}
