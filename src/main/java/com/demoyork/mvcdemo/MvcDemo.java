package com.demoyork.mvcdemo;

// Import the structure MVC objects 
import com.demoyork.mvcdemo.config.cls_MyException;
import com.demoyork.mvcdemo.models.mdl_Category;
import com.demoyork.mvcdemo.models.mdl_Product;
import com.demoyork.mvcdemo.controllers.ctr_Category;
import com.demoyork.mvcdemo.controllers.ctr_Product;

import java.util.ArrayList;
import java.nio.charset.StandardCharsets;
import java.io.*;

public class MvcDemo {

    public static void main(String[] args) throws cls_MyException {
        
        // Create the local category instance 
        ctr_Category cat = new ctr_Category();

        // Testing the insert data process (using de Northwind categories data)
//        cat.ins_Category(new mdl_Category(1, "Beberages", "Soft drinks, coffees, teas, beers, and ales"));
//        cat.ins_Category(new mdl_Category(2, "Condiments", "Sweet and savory sauces, relishes, spreads, and seasonings"));
//        cat.ins_Category(new mdl_Category(3, "Confections", "Desserts, candies, and sweet breads"));
//        cat.ins_Category(new mdl_Category(4, "Dairy products", "Cheeses"));
//        cat.ins_Category(new mdl_Category(5, "Grains/Cereals", "Breads, crackers, pasta, and cereal"));
//        cat.ins_Category(new mdl_Category(6, "Meat/Poultry", "Prepared meats"));
//        cat.ins_Category(new mdl_Category(7, "Confections", "Desserts, candies, and sweet breads"));
//        cat.ins_Category(new mdl_Category(8, "Seafood", "Seaweed and fish"));
//
//        // Insert a new Category
//        cat.ins_Category(new mdl_Category(15, "Fresh", "Vegetables and fruits"));
//
//        
//        // testing the update data process
//        cat.upd_Category(new mdl_Category(15, "Fresh", "Vegetables, fruits and pulps"));
//
//        
//        // testing the delete data process
//        cat.del_Category(15);

        
        // testing the query data process
        ArrayList<mdl_Category> lstData = cat.lst_Category(0);

        if(lstData.size() > 0){
            System.out.println("\nLIST OF CATEGORIES\n");
            System.out.printf("%-4s | %-25s | %-60s\n", "Code","Name", "Description");
            System.out.println("-----+---------------------------+------------------------------------------------------------");
            for (mdl_Category category : lstData) {
                System.out.printf("%4d | %-25s | %-60s\n", category.getCode(),category.getName(),category.getDescription());
            }
            System.out.println("-----+---------------------------+------------------------------------------------------------");
            System.out.println();
        }else{
            System.out.println("Data list is empty");
        }
        
        
        // read content from csv file with products data to store into database
        String datos[], Expre;
        String Direc = System.getProperty("user.dir")+"\\data\\products.csv";
        
        // Create the local category instance 
        ctr_Product prd = new ctr_Product();
//        try{
//            // Create reader object from file
//            BufferedReader br = new BufferedReader(new InputStreamReader(new FileInputStream(Direc),StandardCharsets.UTF_8));
//            
//            // Split data text into chunks and store into Product table
//            while((Expre = br.readLine())!=null) {
//                datos = Expre.split(";");
//                prd.ins_Product(new mdl_Product(Integer.parseInt(datos[0]),
//                                                datos[1],
//                                                Double.parseDouble(datos[2]),
//                                                Integer.parseInt(datos[3])));
//            }
//            
//            // Close buffer
//            br.close();
//        }catch (IOException ex){
//            //Print stack errors
//            ex.printStackTrace();
//        }
//        
        
        // testing the query data process
        ArrayList<mdl_Product> lstDataP = prd.lst_Product(0);

        if(lstDataP.size() > 0){
            System.out.println("\nLIST OF PRODUCTS\n");
            System.out.printf("%-4s | %-50s | %10s\n", "Code","Name", "Price");
            System.out.println("-----+----------------------------------------------------+------------");
            for (mdl_Product product : lstDataP) {
                System.out.printf("%4d | %-50s | %10f\n", product.getCode(), product.getName(),product.getPrice());
            }
            System.out.println("-----+----------------------------------------------------+------------");
            System.out.println();
        }else{
            System.out.println("Data list is empty");
        }
        
        
        // finish demonstration
        System.out.println("Data access demonstration");
        System.out.println(" ");
    }
}
