package com.demoyork.mvcdemo.config;

// import the common libraries
import java.io.*;
import java.util.Properties;

// import the database support libraries
import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.Driver;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;

// import support json data libraries
import org.json.JSONArray;
import org.json.JSONObject;


public class cls_Conexion {
    // local properties
    private String url;
    private String port;
    private String database;
    private String user;
    private String passwd;      
    private Connection conex = null;
    private CallableStatement command = null;
        
    // load database properties 
    private void loadProps(){
        try{
            // retrieve properties file content
            String direc = System.getProperty("user.dir")+"\\resources\\config.properties";
            InputStream input = new FileInputStream(direc);            
            
            // load properties at memory
            Properties props = new Properties();
            props.load(input);
            
            // update local database variables with properties
            this.url = props.getProperty("url");
            this.port = props.getProperty("port");
            this.database = props.getProperty("database");
            this.user = props.getProperty("user");
            this.passwd = props.getProperty("passwd");
            
        }catch(IOException ex){
            System.out.println(ex.getMessage());
        }
    }// end load properties funtion       
    
    // default constructor
    public cls_Conexion(){
        // call load props
        loadProps();
        
        // try to connect with database
        if(conex == null){
            try{                
                Driver driver = new com.mysql.cj.jdbc.Driver();
                DriverManager.registerDriver(driver);
                conex = DriverManager.getConnection("jdbc:mysql://"+url+":"+port+"/"+database, user, passwd);
            }catch (SQLException ex) {
                System.out.println(ex.getMessage());
            }
        }        
    }// end default constructor function
    
    // create SQL instruction
    public void query(String auxSql){
        try{
            command = conex.prepareCall(auxSql);
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
    }// end query function
    
    // binding SQL sentence with required params 
    public void bind(String param, Object value){
        try{
            if(value instanceof String){
               command.setString(param, String.valueOf(value));
            }else if(value instanceof Integer){
               command.setInt(param, Integer.parseInt(String.valueOf(value)));
            }else if(value instanceof Double){
               command.setDouble(param, Double.parseDouble(String.valueOf(value)));
            }                    
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }        
    }// end bind function
    
    // execute SQL instruction
    public boolean execute(){
        boolean status = false;
        try{
            command.execute();
            status = !status;
        }catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return status;
    }// end execute function
    
    // retrieves data collection from database
    // using JSON format
    public JSONArray lstData() {        
        JSONArray lista = new JSONArray();
        String colName;
        try {            
            // retrieves data using Sql query 
            ResultSet Regis = command.executeQuery();
            
            // retrieves metadata from resulset object
            ResultSetMetaData resMeta = Regis.getMetaData();
            int colsCount = resMeta.getColumnCount();
            
            // processing all retrieves records
            while(Regis.next()){
                // creates JSON object
                JSONObject tupla = new JSONObject();
            
                // evaluates each field from data collection
                for(int i = 1; i<= colsCount; i++){
                    colName = resMeta.getColumnName(i);
                    
                    if(resMeta.getColumnType(i) == java.sql.Types.VARCHAR){
                        tupla.put(colName, Regis.getString(colName));
                    }else if((resMeta.getColumnType(i) == java.sql.Types.TINYINT) ||
                             (resMeta.getColumnType(i) == java.sql.Types.SMALLINT) ||
                             (resMeta.getColumnType(i) == java.sql.Types.INTEGER)){
                        tupla.put(colName, Regis.getInt(colName));
                    }else if((resMeta.getColumnType(i) == java.sql.Types.FLOAT)  ||
                             (resMeta.getColumnType(i) == java.sql.Types.DOUBLE) ||
                             (resMeta.getColumnType(i) == java.sql.Types.NUMERIC)||
                             (resMeta.getColumnType(i) == java.sql.Types.DECIMAL)){                             
                        tupla.put(colName, Regis.getDouble(colName));
                    }
                }
                
                // stores the new JSON object into list
                lista.put(tupla);
            }    
            return lista;
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
            return null;
        }            
    }// end lstdata function
}// end class definition
