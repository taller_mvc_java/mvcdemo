package com.demoyork.mvcdemo.config;

public class cls_MyException extends Exception {
    
    // Call super function from base class
    public cls_MyException(){
        super();
    }
    
    // Call super function from base class with custom message
    public cls_MyException(String message){
        super(message);
    }
}
