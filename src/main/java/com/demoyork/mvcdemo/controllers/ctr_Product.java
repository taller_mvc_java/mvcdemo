package com.demoyork.mvcdemo.controllers;

import com.demoyork.mvcdemo.models.mdl_Product;
import com.demoyork.mvcdemo.config.cls_Conexion;
import com.demoyork.mvcdemo.config.cls_MyException;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;

public class ctr_Product {
    // create dabase connection object
    cls_Conexion conex = new cls_Conexion();
    
    // insert a new product into database
    public void ins_Product(mdl_Product data)throws cls_MyException{
        // validates integrity of data
        if(data.getCode() < 0){
            throw new cls_MyException("Negative values are not allowed");
        }
        if(data.getName().length() == 0){
            throw new cls_MyException("Blank values are not allowed");
        }
        if(data.getPrice() < 0){
            throw new cls_MyException("The Price cannot be less than zero");
        }        
        if(data.getCateg() < 0){
            throw new cls_MyException("The value of category are not allowed");
        }
        
        // prepares insert instruction
        conex.query("{call insProduct(?,?,?,?)}");
        
        // binding data to params
        conex.bind("pcode" , data.getCode());
        conex.bind("pname" , data.getName());
        conex.bind("pprice", data.getPrice());
        conex.bind("pcateg", data.getCateg());
        
        // try to execute insert instruction
        if(conex.execute()){
            System.out.println("The data was stored");
        }else{
            System.out.println("The data not was stored");
        }
    }// end insert function
    
    // retrive data from query on category table
    public ArrayList<mdl_Product> lst_Product(int code)throws cls_MyException{
        // validates integrity of data
        if(code < 0){
            throw new cls_MyException("Negative values are not allowed");
        }
        
        // prepares query instruction
        if(code == 0){
            conex.query("Select * from Product");
        }else{
            conex.query("Select * from Product where category = " + code);            
        }
        
        // retrieves data from database
        JSONArray data = conex.lstData();
        
        // prepares data list
        ArrayList<mdl_Product> lstProd = new ArrayList<>();
        data.forEach(element -> {                        
            JSONObject json = new JSONObject(element.toString());
            lstProd.add(new mdl_Product(Integer.parseInt(json.get("code").toString()), 
                                                         json.get("name").toString(), 
                                        Double.parseDouble(json.get("price").toString()),
                                        Integer.parseInt(json.get("category").toString())));
            
        });
        
        // returns data collection
        return lstProd;
    }// end delete function
    
}
