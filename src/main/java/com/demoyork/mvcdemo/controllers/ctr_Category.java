package com.demoyork.mvcdemo.controllers;

import com.demoyork.mvcdemo.models.mdl_Category;
import com.demoyork.mvcdemo.config.cls_Conexion;
import com.demoyork.mvcdemo.config.cls_MyException;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;

public class ctr_Category {
    // create dabase connection object
    cls_Conexion conex = new cls_Conexion();
    
    // insert a new category into database
    public void ins_Category(mdl_Category data)throws cls_MyException{
        // validates integrity of data
        if(data.getCode() < 0){
            throw new cls_MyException("Negative values are not allowed");
        }
        if(data.getName().length() == 0){
            throw new cls_MyException("Blank values are not allowed");
        }
        if(data.getDescription().length() == 0){
            throw new cls_MyException("Blank values are not allowed");
        }
        
        // prepares insert instruction
        conex.query("{call insCategory(?,?,?)}");
        
        // binding data to params
        conex.bind("pcode", data.getCode());
        conex.bind("pname", data.getName());
        conex.bind("pdescription", data.getDescription());
        
        // try to execute insert instruction
        if(conex.execute()){
            System.out.println("The data was stored");
        }else{
            System.out.println("The data not was stored");
        }
    }// end insert function
    
    // update a record of category table
    public void upd_Category(mdl_Category data)throws cls_MyException{
        // validates integrity of data
        if(data.getCode() < 0){
            throw new cls_MyException("Negative values are not allowed");
        }
        if(data.getName().length() == 0){
            throw new cls_MyException("Blank values are not allowed");
        }
        if(data.getDescription().length() == 0){
            throw new cls_MyException("Blank values are not allowed");
        }
        
        // prepares update instruction
        conex.query("{call updCategory(?,?,?)}");
        
        // binding data to params
        conex.bind("pcode", data.getCode());
        conex.bind("pname", data.getName());
        conex.bind("pdescription", data.getDescription());
        
        // try to execute update instruction
        if(conex.execute()){
            System.out.println("The data was updated");
        }else{
            System.out.println("The data not was updated");
        }
    }// end update function
    
    // update a record of category table
    public void del_Category(int code)throws cls_MyException{
        // validates integrity of data
        if(code < 0){
            throw new cls_MyException("Negative values are not allowed");
        }
        
        // prepares update instruction
        conex.query("{call delCategory(?)}");
        
        // binding data to params
        conex.bind("pcode", code);
        
        // try to execute delete instruction
        if(conex.execute()){
            System.out.println("The data was updated");
        }else{
            System.out.println("The data not was updated");
        }
    }// end delete function
    
    // retrive data from query on category table
    public ArrayList<mdl_Category> lst_Category(int code)throws cls_MyException{
        // validates integrity of data
        if(code < 0){
            throw new cls_MyException("Negative values are not allowed");
        }
        
        // prepares query instruction
        if(code == 0){
            conex.query("Select * from Category");
        }else{
            conex.query("Select * from Category where code = " + code);            
        }
        
        // retrieves data from database
        JSONArray data = conex.lstData();
        
        // prepares data list
        ArrayList<mdl_Category> lstCat = new ArrayList<>();
        data.forEach(element -> {                        
            JSONObject json = new JSONObject(element.toString());
            lstCat.add(new mdl_Category(Integer.parseInt(json.get("code").toString()), 
                                                         json.get("name").toString(), 
                                                         json.get("description").toString()));
            
        });
        
        // returns data collection
        return lstCat;
    }// end delete function
}// end category controller
